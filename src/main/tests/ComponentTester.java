import com.aplynk.bootstrap.blueprints.BaseComponent;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.elastic.api.EventEmitter;
import io.elastic.api.ExecutionParameters;
import io.elastic.api.Message;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Created by Blaze on 03-05-2016.
 */
public class ComponentTester {
	public static void main(String... args) {
		EventEmitter.Builder builder = new EventEmitter.Builder();
		builder.onData(new EventEmitter.Callback() {
			@Override
			public void receive(Object data) {
				System.out.println("---| Data Emitted |---");
				Message message = (Message) data;
				JsonObject jsonObject = message.getBody();
				System.out.println(jsonObject.toString());
			}
		});
		builder.onError(new EventEmitter.Callback() {
			@Override
			public void receive(Object data) {
				System.out.println("---| Error Emitted |---");
				Exception exception = (Exception) data;
				System.out.println(exception.getMessage());
			}
		});
		builder.onRebound(new EventEmitter.Callback() {
			@Override
			public void receive(Object data) {
				System.out.println("onRebound : " + data);
			}
		});
		builder.onSnapshot(new EventEmitter.Callback() {
			@Override
			public void receive(Object data) {
				System.out.println("---| Snapshot Emitted |---");
			}
		});
		builder.onUpdateKeys(new EventEmitter.Callback() {
			@Override
			public void receive(Object data) {
				System.out.println("onSnapshot");
			}
		});
		EventEmitter eventEmitter = builder.build();

		try {



			BaseComponent component = (BaseComponent) Class.forName("com.aplynk.bootstrap.triggers.GetOppo").getDeclaredConstructor(EventEmitter.class).newInstance(eventEmitter);



			String inputData = readFile("input.json", StandardCharsets.UTF_8);
			JsonObject input = new Gson().fromJson(inputData, JsonObject.class);
			Message message = new Message.Builder().body(input).build();
			ExecutionParameters.Builder paramBuilder = new ExecutionParameters.Builder(message);

			String configData = readFile("config.json", StandardCharsets.UTF_8);
			JsonObject config = new Gson().fromJson(configData, JsonObject.class);
			 paramBuilder.configuration(config);

			String snapshotData = readFile("snapshot.json", StandardCharsets.UTF_8);
			JsonObject snapshot = new Gson().fromJson(snapshotData, JsonObject.class);
			System.out.println("in tester : Snapshot :" + snapshot.toString());
			paramBuilder.snapshot(snapshot);

			ExecutionParameters params = paramBuilder.build();

			String envVarsData = readFile("env_vars.json", StandardCharsets.UTF_8);
			JsonObject envVars = new Gson().fromJson(envVarsData, JsonObject.class);
			for (Map.Entry<String, JsonElement> item:envVars.entrySet()) {
				component.addMockEnvVar(item.getKey(), item.getValue().getAsString());
			}

			component.execute(params);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String readFile(String path, Charset encoding) throws IOException {
			byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
}
