package com.aplynk.bootstrap.triggers;

import com.aplynk.bootstrap.blueprints.BaseComponent;
import com.aplynk.bootstrap.models.Config;
import com.aplynk.bootstrap.models.InsightlyModule.IContactLinks;
import com.aplynk.bootstrap.models.InsightlyModule.IContacts;

import com.aplynk.bootstrap.models.InsightlyModule.IOpportunities;
import com.aplynk.bootstrap.models.Snapshot;
import com.aplynk.bootstrap.models.standard.AContact;
import com.aplynk.bootstrap.models.standard.ACustomer;
import com.aplynk.bootstrap.models.standard.AnOppoCustomer;
import com.aplynk.bootstrap.models.standard.Opportunity;
import com.aplynk.bootstrap.network.RequestHelper;
import com.aplynk.bootstrap.network.apis.ContactApi;
import com.aplynk.bootstrap.network.apis.OppoApi;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.elastic.api.EventEmitter;
import io.elastic.api.ExecutionParameters;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by lenovo on 18-Jan-17.
 */
public class GetOppo extends BaseComponent {

    public GetOppo(EventEmitter eventEmitter) {
        super(eventEmitter);
    }

    RequestHelper requestHelper = new RequestHelper();


    @Override
    public void execute(ExecutionParameters parameters) {
        Snapshot snapshot = new Gson().fromJson(parameters.getSnapshot(), Snapshot.class);
        Config config=new Gson().fromJson(parameters.getConfiguration(), Config.class);

        log(config.toString());
        String apiKey =config.getApiKey();
        OppoApi api = requestHelper.getBuilder(apiKey).create(OppoApi.class);
        ContactApi capi = requestHelper.getBuilder(apiKey).create(ContactApi.class);
        List<IOpportunities> response = api.getOppo();

        for (int i = 0; i < response.size(); i++) {


            String OppId = response.get(i).getOPPORTUNITYID();
            IOpportunities res = api.getOppobyID(OppId);
            String CustID = res.getLINKS().get(0).getCONTACT_ID();//res.getLINKS().get(0).getCONTACT_ID();
           if(CustID!=null) {
               IContacts res1 = capi.getContactbyid(CustID);
               ACustomer customer = new ACustomer(res1.getCONTACT_ID(), res1.getName(), res1.getCONTACTINFOS());
               AnOppoCustomer opp = new AnOppoCustomer(res.getOPPORTUNITYID(), res.getOPPORTUNITYNAME(), res.getBID_AMOUNT(), res.getFORECASTCLOSEDATE(), customer);
               emitData(opp.toJson());
           }
           else {
               AnOppoCustomer opp = new AnOppoCustomer(res.getOPPORTUNITYID(), res.getOPPORTUNITYNAME(), res.getBID_AMOUNT(), res.getFORECASTCLOSEDATE(), null);
               emitData(opp.toJson());
           }

        }
    }
}



