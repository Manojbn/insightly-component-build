package com.aplynk.bootstrap.triggers;

import com.aplynk.bootstrap.blueprints.BaseComponent;

import com.aplynk.bootstrap.models.InsightlyModule.Ileads;
import com.aplynk.bootstrap.models.standard.ALead;
import com.aplynk.bootstrap.network.RequestHelper;
import com.aplynk.bootstrap.network.apis.LeadsApi;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.elastic.api.EventEmitter;
import io.elastic.api.ExecutionParameters;

import java.util.List;

/**
 * Created by Manoj N Bisarahalli on 17-Jan-17.
 */
public class GetLeads extends BaseComponent{

    public GetLeads(EventEmitter eventEmitter) {
        super(eventEmitter);
    }

    RequestHelper requestHelper=new RequestHelper();
    private LeadsApi api;

    @Override
    public void execute(ExecutionParameters parameters) {

        String token=getEnv("Username");
        api = requestHelper.getBuilder(token).create(LeadsApi.class);
        ALead res = new ALead();


        List<Ileads> ilp = api.getLeads();
        for(int i=0;i<ilp.size();i++)
        {
            res=new ALead(ilp.get(i).getId(),ilp.get(i).getName(),ilp.get(i).getwebsite(),ilp.get(i).getphone(),ilp.get(i).getbilling_country(),ilp.get(i).getbilling_state(),ilp.get(i).getbilling_city(),ilp.get(i).getbilling_street());
        }

        log(ilp.toString());
        emitData(res.toJson());
    }
}
