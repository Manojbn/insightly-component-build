package com.aplynk.bootstrap.triggers;

import com.aplynk.bootstrap.blueprints.BaseComponent;

import com.aplynk.bootstrap.models.InsightlyModule.IAddress;
import com.aplynk.bootstrap.models.InsightlyModule.IContacts;
import com.aplynk.bootstrap.models.standard.AContact;
import com.aplynk.bootstrap.network.RequestHelper;
import com.aplynk.bootstrap.network.apis.ContactApi;
import io.elastic.api.EventEmitter;
import io.elastic.api.ExecutionParameters;

import java.util.List;

/**
 * Created by lenovo on 19-Jan-17.
 */
public class GetContacts extends BaseComponent {

    public GetContacts(EventEmitter eventEmitter) {
        super(eventEmitter);
    }

    RequestHelper requestHelper = new RequestHelper();
    private ContactApi api;


    @Override
    public void execute(ExecutionParameters parameters) {
        String token = getEnv("Username");
        api = requestHelper.getBuilder(token).create(ContactApi.class);
        AContact ic = new AContact();
        List<IContacts> res = api.getContact();


        for (int i = 0; i < res.size(); i++) {
            ic = new AContact(res.get(i).getCONTACT_ID(), res.get(i).getName());
        }

        log("IC : " + ic);
//                log(ic.toString());
        log(res.toString());
        emitData(ic.toJson());

    }




    }



