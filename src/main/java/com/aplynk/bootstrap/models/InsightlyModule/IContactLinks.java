package com.aplynk.bootstrap.models.InsightlyModule;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;

/**
 * Created by Manoj N Bisarahalli on 27-Jan-17.
 */
public class IContactLinks extends JsonModel {

    private String CONTACT_ID;

    public String getCONTACT_ID() {
        return CONTACT_ID;
    }

    public void setCONTACT_ID(String CONTACT_ID) {
        this.CONTACT_ID = CONTACT_ID;
    }

    public String getOPPORTUNITY_ID() {
        return OPPORTUNITY_ID;
    }

    public void setOPPORTUNITY_ID(String OPPORTUNITY_ID) {
        this.OPPORTUNITY_ID = OPPORTUNITY_ID;
    }

    public String getROLE() {
        return ROLE;
    }

    public void setROLE(String ROLE) {
        this.ROLE = ROLE;
    }

    private String OPPORTUNITY_ID;
    private String ROLE;


    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {
        return getJson (this);
    }




}
