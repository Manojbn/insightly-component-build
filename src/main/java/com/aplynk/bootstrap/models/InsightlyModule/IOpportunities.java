package com.aplynk.bootstrap.models.InsightlyModule;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;

import java.util.List;

/**
 * Created by Manoj N Bisarahalli on 24-Jan-17.
 */
public class IOpportunities extends JsonModel {

        private List<IContactLinks> LINKS;


        public List<IContactLinks> getLINKS() {
                return LINKS;
        }

        public void setLINKS(List<IContactLinks> LINKS) {
                this.LINKS = LINKS;
        }

        private  Integer BID_AMOUNT;

        public Integer getBID_AMOUNT() {
                return BID_AMOUNT;
        }

        public void setBID_AMOUNT(Integer BID_AMOUNT) {
                this.BID_AMOUNT = BID_AMOUNT;
        }





        private String OPPORTUNITY_ID;

        public String getOPPORTUNITYID() { return this.OPPORTUNITY_ID; }



        private String OPPORTUNITY_NAME;

        public String getOPPORTUNITYNAME() { return this.OPPORTUNITY_NAME; }


        private String OPPORTUNITY_DETAILS;

        public String getOPPORTUNITYDETAILS() { return this.OPPORTUNITY_DETAILS; }


        private String FORECAST_CLOSE_DATE;

        public String getFORECASTCLOSEDATE() { return this.FORECAST_CLOSE_DATE; }


        private String ACTUAL_CLOSE_DATE;

        public String getACTUALCLOSEDATE() { return this.ACTUAL_CLOSE_DATE; }


        private String OPPORTUNITY_STATE;

        public String getOPPORTUNITYSTATE() { return this.OPPORTUNITY_STATE; }


        private String DATE_CREATED_UTC;

        public String getDATECREATEDUTC() { return this.DATE_CREATED_UTC; }


        private String DATE_UPDATED_UTC;

        public String getDATEUPDATEDUTC() { return this.DATE_UPDATED_UTC; }
        @Override
        public String toString()
        {
            return toJson().toString();
        }

        @Override
        public JsonObject toJson() {

            return getJson(this);
        }



}
