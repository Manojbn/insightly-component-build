package com.aplynk.bootstrap.models.InsightlyModule;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Manoj N Bisarahalli on 27-Jan-17.
 */
public class IContactInfo extends JsonModel {


    @SerializedName("DETAIL")
    private String Emailid;
    public String getDETAIL() {
        return Emailid;
    }
    public void setDETAIL(String DETAIL) {
        this.Emailid = DETAIL;
    }


    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {
        return getJson (this);
    }



}
