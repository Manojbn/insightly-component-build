package com.aplynk.bootstrap.models.InsightlyModule;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;

/**
 * Created by lenovo on 20-Jan-17.
 */
public class IAddress extends JsonModel {

    private String ADDRESS_ID;

    private String STATE;

    private String COUNTRY;

    private String POSTCODE;

    private String ADDRESS_TYPE;

    private String CITY;

    private String STREET;

    public String getADDRESS_ID ()
    {
        return ADDRESS_ID;
    }

    public void setADDRESS_ID (String ADDRESS_ID)
    {
        this.ADDRESS_ID = ADDRESS_ID;
    }

    public String getSTATE ()
    {
        return STATE;
    }



    public void setSTATE (String STATE)
    {
        this.STATE = STATE;
    }

    public String getCOUNTRY ()
    {
        return COUNTRY;
    }

    public void setCOUNTRY (String COUNTRY)
    {
        this.COUNTRY = COUNTRY;
    }

    public String getPOSTCODE ()
    {
        return POSTCODE;
    }

    public void setPOSTCODE (String POSTCODE)
    {
        this.POSTCODE = POSTCODE;
    }

    public String getADDRESS_TYPE ()
    {
        return ADDRESS_TYPE;
    }

    public void setADDRESS_TYPE (String ADDRESS_TYPE)
    {
        this.ADDRESS_TYPE = ADDRESS_TYPE;
    }

    public String getCITY ()
    {
        return CITY;
    }

    public void setCITY (String CITY)
    {
        this.CITY = CITY;
    }

    public String getSTREET ()
    {
        return STREET;
    }


    public void setSTREET (String STREET)
    {
        this.STREET = STREET;
    }

    @Override
    public String toString()
    {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {

        return getJson(this);
    }
}
