package com.aplynk.bootstrap.models.InsightlyModule;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Manoj N Bisarahalli on 20-Jan-17.
 */
public class Ileads extends JsonModel {

    @SerializedName("LEAD_ID")
    private String id;

    private String Name;

    private String LAST_NAME;
    private String FIRST_NAME;

    //List<String> email;

    private String WEBSITE_URL;

    private String PHONE_NUMBER;

    private String ADDRESS_COUNTRY;
    private String ADDRESS_STATE;
    private String ADDRESS_CITY;
    private String ADDRESS_STREET;


    public String getLname ()
    {
        return LAST_NAME;
    }
    public String getFname ()
    {
        return FIRST_NAME;
    }
    public String getName() {
        String fname = getFname();
        String lname = getLname();
        Name = fname + " " + lname;
        return Name;
    }




    public String getId() {
        return id;
    }

    public String getwebsite ()
    {
        return WEBSITE_URL;
    }



    public void setwebsite (String website)
    {
        this.WEBSITE_URL = website;
    }


    public String getphone ()
    {
        return PHONE_NUMBER;
    }
    public void setphone (String phone)
    {
        this.PHONE_NUMBER = phone;
    }

    public String getbilling_country ()
    {
        return ADDRESS_COUNTRY;
    }
    public void setbilling_country (String billing_country)
    {
        this.ADDRESS_COUNTRY = billing_country;
    }

    public String getbilling_city ()
    {
        return ADDRESS_CITY;
    }
    public void setbilling_city (String billing_city)
    {
        this.ADDRESS_CITY = billing_city;
    }

    public String getbilling_state()
    {
        return ADDRESS_STATE;
    }
    public void setbilling_state (String billing_state)
    {
        this.ADDRESS_STATE = billing_state;
    }

    public String getbilling_street ()
    {
        return ADDRESS_STREET ;
    }
    public void setbilling_street (String billing_street)
    {
        this.ADDRESS_STREET = billing_street;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {
        return getJson(this);
    }
}
