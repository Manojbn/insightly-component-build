package com.aplynk.bootstrap.models.InsightlyModule;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lenovo on 20-Jan-17.
 */
public class IContacts extends JsonModel {

    @SerializedName("CONTACT_ID")
    private String id;

    public String getCONTACT_ID() {
        return id;
    }

    public void setCONTACT_ID(String CONTACT_ID) {
        this.id = CONTACT_ID;
    }

    private String Name;

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    @SerializedName("LAST_NAME")
    private String lname;

    @SerializedName("FIRST_NAME")
    private String fname;

    @SerializedName("CONTACTINFOS")
    private List<IContactInfo> Emails;
    public List<IContactInfo> getCONTACTINFOS() {
        return Emails;
    }

    public void setCONTACTINFOS(List<IContactInfo> CONTACTINFOS) {
        this.Emails = CONTACTINFOS;
    }




    public String getLAST_NAME() {
        return lname;
    }

    public String getFIRST_NAME() {
        return fname;
    }

    public String getName() {
        String fname = getFIRST_NAME();
        String lname = getLAST_NAME();
        Name = fname + " " + lname;
        return Name;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {
        return getJson (this);
    }


}
