package com.aplynk.bootstrap.models.standard;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.aplynk.bootstrap.models.InsightlyModule.IAddress;
import com.aplynk.bootstrap.models.InsightlyModule.IContacts;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lenovo on 24-Jan-17.
 */
public class AContact extends JsonModel {

    private String id;

    private String Name;



    private List<IAddress> ADDRESSES;

    public AContact(String id, String name) {
        this.id = id;
        Name = name;

    }

    public AContact() {
    }

    public String getid ()
    {
        return id;
    }



    public String getName ()
    {
        return Name;
    }


   /* public String getAddress()
    {
        return Address;
    }*/
    public List<IAddress> getADDRESSES()
    {
        return ADDRESSES;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }


    @Override
    public JsonObject toJson() {
        return getJson(this);
    }
}
