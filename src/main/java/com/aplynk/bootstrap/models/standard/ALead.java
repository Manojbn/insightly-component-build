package com.aplynk.bootstrap.models.standard;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Manoj N Bisarahalli on 24-Jan-17.
 */
public class ALead extends JsonModel{

    private String id;
    private String name;
    private String website;
    private String phone;


    @SerializedName("billing_country")
    private String billingCountry;
    @SerializedName("billing_state")
    private String billingState;
    @SerializedName("billing_city")
    private String billingCity;
    @SerializedName("billing_street")
    private String billingStreet;

    public ALead(String id, String name, String website, String phone, String billingCountry, String billingState, String billingCity, String billingStreet) {
        this.id = id;
        this.name = name;
        this.website = website;
        this.phone = phone;
        this.billingCountry = billingCountry;
        this.billingState = billingState;
        this.billingCity = billingCity;
        this.billingStreet = billingStreet;
    }

    public ALead() {
    }


    public void checkValidity() {
        if(!isValid()) {
            throw new RuntimeException("Lead invalid");
        }
    }

    private boolean isValid() {
        return name != null && name.length() > 0 && id != null && id.length() > 0;
    }

    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getWebsite() {
        return website;
    }
    public String getPhone() {
        return phone;
    }
    public String getBillingCountry() {
        return billingCountry;
    }
    public String getBillingState() {
        return billingState;
    }
    public String getBillingCity() {
        return billingCity;
    }
    public String getBillingStreet() {
        return billingStreet;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {
        return getJson(this);
    }



}
