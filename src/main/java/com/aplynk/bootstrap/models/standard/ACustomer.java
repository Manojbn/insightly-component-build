package com.aplynk.bootstrap.models.standard;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.aplynk.bootstrap.models.InsightlyModule.IContactInfo;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Soumya on 05-12-2016.
 */
public class ACustomer extends JsonModel {




    private String id;


    private String name;

    private  String email;

    public ACustomer(String id, String name, List<IContactInfo> email) {
        this.id = id;
        this.name = name;
        this.email = email.get(0).getDETAIL();
    }


    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {
        return getJson(this);
    }
}
