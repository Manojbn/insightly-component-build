package com.aplynk.bootstrap.models.standard;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;

import java.util.List;

/**
 * Created by lenovo on 19-Jan-17.
 */
public class Opportunity extends JsonModel {
    private String OPPORTUNITY_ID;


    public Opportunity(String OPPORTUNITY_ID, String OPPORTUNITY_NAME, String OPPORTUNITY_DETAILS, String FORECAST_CLOSE_DATE, String ACTUAL_CLOSE_DATE, String OPPORTUNITY_STATE, String DATE_CREATED_UTC, String DATE_UPDATED_UTC) {
        this.OPPORTUNITY_ID = OPPORTUNITY_ID;
        this.OPPORTUNITY_NAME = OPPORTUNITY_NAME;
        this.OPPORTUNITY_DETAILS = OPPORTUNITY_DETAILS;
        this.FORECAST_CLOSE_DATE = FORECAST_CLOSE_DATE;
        this.ACTUAL_CLOSE_DATE = ACTUAL_CLOSE_DATE;
        this.OPPORTUNITY_STATE = OPPORTUNITY_STATE;
        this.DATE_CREATED_UTC = DATE_CREATED_UTC;
        this.DATE_UPDATED_UTC = DATE_UPDATED_UTC;
    }

    public Opportunity() {
    }

    public String getOPPORTUNITYID() { return this.OPPORTUNITY_ID; }



    private String OPPORTUNITY_NAME;

    public String getOPPORTUNITYNAME() { return this.OPPORTUNITY_NAME; }


    private String OPPORTUNITY_DETAILS;

    public String getOPPORTUNITYDETAILS() { return this.OPPORTUNITY_DETAILS; }


    private String FORECAST_CLOSE_DATE;

    public String getFORECASTCLOSEDATE() {

        String Forcast_close_date=FORECAST_CLOSE_DATE.replace(' ','T');
        return Forcast_close_date; }


    private String ACTUAL_CLOSE_DATE;

    public String getACTUALCLOSEDATE() { return this.ACTUAL_CLOSE_DATE; }


    private String OPPORTUNITY_STATE;

    public String getOPPORTUNITYSTATE() { return this.OPPORTUNITY_STATE; }


    private String DATE_CREATED_UTC;

    public String getDATECREATEDUTC() { return this.DATE_CREATED_UTC; }


    private String DATE_UPDATED_UTC;

    public String getDATEUPDATEDUTC() { return this.DATE_UPDATED_UTC; }
    @Override
    public String toString()
    {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {

        return getJson(this);
    }
}
