package com.aplynk.bootstrap.models.standard;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.google.gson.JsonObject;

/**
 * Created by lenovo on 19-Jan-17.
 */
public class Leads extends JsonModel {
    private String id;

    private String Name;

    private String Lname;
    private String Fname;

    private String email;

    private String website;

    private String phone;

    private String billing_country;
    private String billing_state;
    private String billing_city;
    private String billing_street;


    public String getid ()
    {
        return id;
    }
    public void setid (String id)
    {
        this.id = id;
    }

    public String Lname ()
    {
        return Lname;
    }
    public String Fname ()
    {
        return Fname;
    }
    public String Name() {
        String fname = Fname();
        String lname = Lname();
        Name = fname + " " + lname;
        return Name;
    }
    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getemail ()
    {
        return email;
    }
    public void setemail (String email)
    {
        this.email = email;
    }


    public String getwebsite ()
    {
        return website;
    }



    public void setwebsite (String website)
    {
        this.website = website;
    }


    public String getphone ()
    {
        return phone;
    }
    public void setphone (String phone)
    {
        this.phone = phone;
    }

    public String getbilling_country ()
    {
        return billing_country;
    }
    public void setbilling_country (String billing_country)
    {
        this.billing_country = billing_country;
    }

    public String getbilling_city ()
    {
        return billing_city;
    }
    public void setbilling_city (String billing_city)
    {
        this.billing_city = billing_city;
    }

    public String getbilling_state()
    {
        return billing_state;
    }
    public void setbilling_state (String billing_state)
    {
        this.billing_state = billing_state;
    }

    public String getbilling_street ()
    {
        return billing_street ;
    }
    public void setbilling_street (String billing_street)
    {
        this.billing_street = billing_street;
    }



    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public JsonObject toJson() {
        return null;
    }
}
