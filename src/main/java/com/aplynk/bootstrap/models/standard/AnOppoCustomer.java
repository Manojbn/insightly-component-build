package com.aplynk.bootstrap.models.standard;

import com.aplynk.bootstrap.blueprints.JsonModel;
import com.aplynk.bootstrap.models.InsightlyModule.IContacts;
import com.google.gson.JsonObject;

/**
 * Created by Manoj N Bisarahalli on 27-Jan-17.
 */
public class AnOppoCustomer extends JsonModel {

       private String id;
        private String name;
        private  Integer amount;
        private String closedate;

        public AnOppoCustomer(String OPPORTUNITY_ID, String OPPORTUNITY_NAME, Integer BID_AMOUNT, String FORECAST_CLOSE_DATE, ACustomer Customer) {
            this.id = OPPORTUNITY_ID;
            this.name = OPPORTUNITY_NAME;
            this.amount = BID_AMOUNT;
            this.closedate = FORECAST_CLOSE_DATE;
            this.customer = Customer;
        }


        private ACustomer customer;

        @Override
        public String toString() {
            return toJson().toString();
        }

        @Override
        public JsonObject toJson() {
            return getJson (this);
        }














}
