package com.aplynk.bootstrap.network.apis;


import com.aplynk.bootstrap.models.InsightlyModule.Ileads;
import com.google.gson.JsonArray;
import retrofit.http.GET;

import java.util.List;

/**
 * Created by Manoj N Bisarahalli on 17-Jan-17.
 */
public interface LeadsApi {
    @GET("/Leads")
    List<Ileads> getLeads();
}
