package com.aplynk.bootstrap.network.apis;

import com.aplynk.bootstrap.models.InsightlyModule.IOpportunities;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

import java.util.List;

/**
 * Created by lenovo on 18-Jan-17.
 */
public interface OppoApi {
    @GET("/Opportunities")
    List<IOpportunities> getOppo();

    @GET("/Opportunities")
    List<IOpportunities> getoppo(@Query("$filter") String filter);

    @GET("/Opportunities/{id}")
    IOpportunities getOppobyID(@Path("id")String id);

}

