package com.aplynk.bootstrap.network.apis;

import com.aplynk.bootstrap.models.InsightlyModule.IAddress;
import com.aplynk.bootstrap.models.InsightlyModule.IContacts;
import com.aplynk.bootstrap.models.standard.ACustomer;
import com.google.gson.JsonArray;
import retrofit.http.GET;
import retrofit.http.Path;

import java.util.List;

/**
 * Created by lenovo on 19-Jan-17.
 */
public interface ContactApi {

    @GET("/contacts/{id}")
    IContacts getContactbyid(@Path("id") String id);

    @GET("/contacts")
    List<IContacts> getContact();
}


