package com.aplynk.bootstrap.network;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import org.apache.commons.codec.binary.Base64;


/**
 * Created by soumya on 19-04-2016.
 */
public class RequestHelper {
    //TODO: Change the base url when implementing
    private static final String BASE_URL = "https://api.insight.ly/v2.1";

    public RestAdapter getBuilder(String username) {

        username = username + ":";
        byte[] encodedBytes = Base64.encodeBase64(username.getBytes());
        final String token = new String(encodedBytes);
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(BASE_URL);
        builder.setLogLevel(RestAdapter.LogLevel.FULL);
        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {


                                request.addHeader("Accept", "application/json");
                                request.addHeader("Authorization", "Basic " + token);
                            }


        });
        return builder.build();

    }
}
